# .tmux.conf - zblach 2013

# begin counting at '1' instead of '0'
set -g base-index 1

set -g message-fg colour16
set -g message-bg colour221
set -g message-attr bold

# set status line info
set -g status-utf8 on

set -g status-fg white
set -g status-bg colour234
set -g status-left-length 9999
set -g status-left '#[fg=colour235,bg=colour252,bold] #S #[fg=colour252,bg=colour238,nobold]⮀#[fg=colour245,bg=colour238,bold] #(whoami) #{?pane_synchronized,#[fg=colour238]#[bg=colour166]⮀#[fg=default] SYNC #[fg=colour166],#[fg=colour238]}#[bg=default,nobold]⮀'
set -g status-right-length 9999 
set -g status-right "#[bg=color235]⮃ #T #[fg=colour166]#[fg=colour10,nobold]⮂#[fg=colour232,bg=colour10] %d/%m/%y #[fg=colour13]⮂#[fg=colour232,bg=colour13,nobold] %H:%M #[default]"

set -g window-status-format "#[fg=white,bg=default]#I #W "
set -g window-status-current-format "#[fg=colour234,bg=colour39]⮀ #I #W:#{?pane_synchronized,*,#P} #[reverse]⮀ "
#set -g window-status-separator " "


# easy accessibility bindings
#bind -n S-down new-window
#bind -n S-left prev
#bind -n S-right next
#bind -n C-S-left swap-window -t -1
#bind -n C-S-right swap-window -t +1

# mouse settings
set -g mode-mouse off
set -g mouse-select-window on
set -g mouse-select-pane on


### 
set-option -sg escape-time 0
set-option allow-rename on
set-option -g automatic-rename on

#### COLOUR

# bell info
set -g window-status-bell-attr bright
set -g window-status-bell-bg   "colour12"

# activity info 
set-window-option -g window-status-activity-attr dim
set-window-option -g window-status-activity-bg "colour13"

# content info
set-window-option -g window-status-content-attr blink
set-window-option -g window-status-content-bg "colour9"

# default window title colors
set-window-option -g window-status-fg colour244
set-window-option -g window-status-bg default
set-window-option -g window-status-attr dim

# active window title colors
set-window-option -g window-status-current-fg colour166 #orange
set-window-option -g window-status-current-bg default
set-window-option -g window-status-current-attr bright

# pane border
set-option -g pane-border-fg colour235 #base02
set-option -g pane-active-border-fg red #base01

# message text
set-option -g message-bg colour235 #base02
set-option -g message-fg colour166 #orange

# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange

# clock
set-window-option -g clock-mode-colour colour64 #green



#### Rebindings to emuluate 'screen' behaviour

# Set the prefix to ^A.
unbind C-b
set -g prefix ^A
bind a send-prefix

# lockscreen ^X x 
unbind ^X
bind ^X lock-server
unbind x
bind x lock-server

# screen ^C c 
unbind ^C
bind ^C new-window 
bind c new-window -c '#{pane_current_path}'

# detach ^D d
unbind ^D
bind ^D detach

# displays * 
unbind *
bind * list-clients

# next ^@ ^N sp n 
unbind ^@
bind ^@ next-window
unbind ^N
bind ^N next-window
unbind " "
bind " " next-window
unbind n
bind n next-window

# title A
unbind A
bind A command-prompt "rename-window %%"

# other ^A
unbind ^A
bind ^A last-window

# prev ^H ^P p ^? 
unbind ^H
bind ^H previous-window
unbind ^P
bind ^P previous-window
unbind p
bind p previous-window
unbind BSpace
bind BSpace previous-window

# windows ^W w 
unbind ^W
bind ^W list-windows
unbind w
bind w list-windows

# kill K k 
unbind K
bind K confirm-before "kill-window"
unbind k
bind k confirm-before "kill-pane"

# redisplay ^L l 
unbind ^L
bind ^L refresh-client
unbind l
bind l refresh-client

# split -v |
unbind |
bind | split-window -h -c '#{pane_current_path}'

unbind %
bind _ split-window -v -c '#{pane_current_path}'

# " windowlist -b
unbind '"'
bind '"' choose-window

# vim-like copy/paste
unbind [
bind Escape copy-mode
bind-key -t vi-copy 'v' begin-selection
bind-key -t vi-copy 'y' copy-selection

# synchronize-panes
bind C-S confirm-before -p "toggle synchronize?" "setw synchronize-pane"

#set -g @tpm_plugins "                   \
#    tmux-plugins/tpm                    \
#    tmux-plugins/tmux-sensible          \
#    tmux-plugins/tmux-copycat           \
#    tmux-plugins/tmux-yank              \
#    tmux-plugins/tmux-open              \
#    tmux-plugins/tmux-resurrect         \
#"

#run-shell ~/.tmux/plugins/tpm/tpm
