" .gvimrc - zblach 2013
" vim: set foldmarker={{{,}}} foldmethod=marker tabstop=2 nospell
set guifont=Monaco:h9
set guioptions=agimtT
let b:colorscheme='base16-monokai'
