" .vimrc - zblach 2014
" vim: fmr={{{,}}} fdm=marker

" vim meta-setup {{{
set nocompatible
filetype off
au! BufWritePost .vimrc source %

let g:colorscheme='Tomorrow-Night'

" }}}

" startup {{{
syntax on
exe "colorscheme ".g:colorscheme
" }}}
